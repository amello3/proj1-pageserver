# Proj0-Hello
-------------

#Author:
--------
-Austin Mello

#Contact Information
--------------------
- Email: amello3@uoregon.edu

- Phone: 530-276-1662

- Zoom: amello3@uoregon.edu

#Description:
-------------
- Simple Web server. 
- Listens to a port and creates a socket.
- Takes an input from the user and outputs accordingly.
